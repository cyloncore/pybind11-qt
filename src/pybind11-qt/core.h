#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <QString>
#include <QVariant>

namespace pybind11_qt
{
  template<typename _T_>
  void register_qvariant_converter();
  namespace detail
  {
    struct abstract_qvariant_converter;
    struct qvariant_converter_interface
    {
      template<typename _T_>
      friend void pybind11_qt::register_qvariant_converter();
      friend struct pybind11::detail::type_caster<QVariant>;
    private:
      static void register_qvariant_converter(abstract_qvariant_converter* _converter);
      static QList<const detail::abstract_qvariant_converter*> converters();
      static pybind11::handle to_python(const QVariant& _variant, pybind11::return_value_policy _policy, pybind11::handle _parent);
      static QVariant to_variant(pybind11::handle _handle);
    };
    struct abstract_qvariant_converter
    {
      virtual bool can_cast(const QVariant& _variant, bool _strict) const = 0;
      virtual pybind11::handle to_python(const QVariant& _variant, pybind11::return_value_policy _policy, pybind11::handle _parent) const = 0;
      virtual QVariant to_variant(pybind11::handle _handle) const = 0;
    };


    inline pybind11::handle qvariant_converter_interface::to_python(const QVariant& _variant, pybind11::return_value_policy _policy, pybind11::handle _parent)
    {
      for(const abstract_qvariant_converter* conv : converters())
      {
        if(conv->can_cast(_variant, true))
        {
          return conv->to_python(_variant, _policy, _parent);
        }
      }
      for(const abstract_qvariant_converter* conv : converters())
      {
        if(conv->can_cast(_variant, false))
        {
          return conv->to_python(_variant, _policy, _parent);
        }
      }
      throw pybind11::type_error("No converter for type: " + std::string(_variant.typeName()) + " to Python from QVariant.");
    }

    inline QVariant qvariant_converter_interface::to_variant(pybind11::handle _handle)
    {
      for(const abstract_qvariant_converter* conv : converters())
      {
        try
        {
          return conv->to_variant(_handle);
        } catch(const std::exception&)
        {}
      }
      throw ::pybind11::type_error("No converter for python value '" + std::string(::pybind11::repr(_handle)) + "' to qvariant.");
    }


    template<typename _T_>
    struct qvariant_converter : public abstract_qvariant_converter
    {
      bool can_cast(const QVariant& _variant, bool _strict) const override
      {
        if(_strict)
        {
          return _variant.userType() == qMetaTypeId<_T_>();
        } else {
          return _variant.canConvert<_T_>();
        }
      }
      pybind11::handle to_python(const QVariant& _variant, pybind11::return_value_policy _policy, pybind11::handle _parent) const override
      {
        return pybind11::cast(_variant.value<_T_>(), _policy, _parent).release();
      }
      QVariant to_variant(pybind11::handle _handle) const override
      {
        return QVariant::fromValue(pybind11::cast<_T_>(_handle));
      }
    };

  }
  template<typename _T_>
  void register_qvariant_converter()
  {
    detail::qvariant_converter_interface::register_qvariant_converter(new detail::qvariant_converter<_T_>());
  }
}

//BEGIN OVERLOAD
namespace pybind11_qt
{
  namespace detail
  {
    template <typename... Args>
    struct non_const_overload
    {
      template <typename R, typename C>
      constexpr auto operator()(R (C::*ptr)(Args...)) const noexcept -> decltype(ptr)
      { return ptr; }
      
      template <typename R, typename C>
      static constexpr auto of(R (C::*ptr)(Args...)) noexcept -> decltype(ptr)
      { return ptr; }
    };
    
    template <typename... Args>
    struct const_overload
    {
      template <typename R, typename C>
      constexpr auto operator()(R (C::*ptr)(Args...) const) const noexcept -> decltype(ptr)
      { return ptr; }
      
      template <typename R, typename C>
      static constexpr auto of(R (C::*ptr)(Args...) const) noexcept -> decltype(ptr)
      { return ptr; }
    };
    
    template <typename... Args>
    struct overload : const_overload<Args...>, non_const_overload<Args...>
    {
      using const_overload<Args...>::of;
      using const_overload<Args...>::operator();
      using non_const_overload<Args...>::of;
      using non_const_overload<Args...>::operator();
      
      template <typename R>
      constexpr auto operator()(R (*ptr)(Args...)) const noexcept -> decltype(ptr)
      { return ptr; }
      
      template <typename R>
      static constexpr auto of(R (*ptr)(Args...)) noexcept -> decltype(ptr)
      { return ptr; }
    };
    template <typename R, typename C, typename... Args>
    struct non_const_overload_r_c
    {
      constexpr auto operator()(R (C::*ptr)(Args...)) const noexcept -> decltype(ptr)
      { return ptr; }
      
      static constexpr auto of(R (C::*ptr)(Args...)) noexcept -> decltype(ptr)
      { return ptr; }
    };
    
    template <typename R, typename C, typename... Args>
    struct const_overload_r_c
    {
      constexpr auto operator()(R (C::*ptr)(Args...) const) const noexcept -> decltype(ptr)
      { return ptr; }
      
      static constexpr auto of(R (C::*ptr)(Args...) const) noexcept -> decltype(ptr)
      { return ptr; }
    };
    
    template <typename R, typename C, typename... Args>
    struct overload_r_c : const_overload_r_c<R, C, Args...>, non_const_overload_r_c<R, C, Args...>
    {
      using const_overload_r_c<R, C, Args...>::of;
      using const_overload_r_c<R, C, Args...>::operator();
      using non_const_overload_r_c<R, C, Args...>::of;
      using non_const_overload_r_c<R, C, Args...>::operator();
      
      constexpr auto operator()(R (*ptr)(Args...)) const noexcept -> decltype(ptr)
      { return ptr; }
      
      static constexpr auto of(R (*ptr)(Args...)) noexcept -> decltype(ptr)
      { return ptr; }
    };

  }

  template <typename... Args> constexpr __attribute__((__unused__)) detail::overload_r_c<Args...> overload_r_c = {};
  template <typename... Args> constexpr __attribute__((__unused__)) detail::overload<Args...> overload = {};
  template <typename... Args> constexpr __attribute__((__unused__)) detail::const_overload<Args...> const_overload = {};
  template <typename... Args> constexpr __attribute__((__unused__)) detail::non_const_overload<Args...> non_const_overload = {};
}
//END OVERLOAD

namespace pybind11::detail
{
  template <>
  struct type_caster<QString>
  {
  public:
    PYBIND11_TYPE_CASTER(QString, const_name("QString"));


    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      if(src.is_none())
      {
        value = QString();
        return true;
      } else if(pybind11::isinstance<pybind11::str>(src))
      {
        value = QString::fromStdString(pybind11::cast<std::string>(src));
        return true;
      } else {
        return false;
      }
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(QString src, return_value_policy policy, handle parent ) {
      return pybind11::cast(src.toStdString(), policy, parent).release();
    }
  };
  template <>
  struct type_caster<QByteArray>
  {
  public:
    PYBIND11_TYPE_CASTER(QByteArray, const_name("QByteArray"));
    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      if(src.is_none())
      {
        value = QByteArray();
        return true;
      } else if(isinstance<pybind11::bytes>(src))
      {
        pybind11::bytes src_bytes = pybind11::reinterpret_borrow<pybind11::bytes>(src);

        char *buffer = nullptr;
        ssize_t length = 0;
        if (PYBIND11_BYTES_AS_STRING_AND_SIZE(src_bytes.ptr(), &buffer, &length))
        {
          pybind11_fail("Unable to extract bytes contents!");
        }
        value = QByteArray(buffer, length);
        return true;
      } else {
        return false;
      }
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(QByteArray src, return_value_policy, handle  ) {
      return pybind11::bytes(src.data(), src.size()).release();
    }
  };
  template <>
  struct type_caster<QVariant>
  {
  public:
    PYBIND11_TYPE_CASTER(QVariant, const_name("QVariant"));


    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      value = pybind11_qt::detail::qvariant_converter_interface::to_variant(src);
      return true;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(QVariant src, return_value_policy policy, handle parent ) {
      return pybind11_qt::detail::qvariant_converter_interface::to_python(src, policy, parent);
    }
  };
  template <typename _T_>
  struct type_caster<QFlags<_T_>>
  {
    PYBIND11_TYPE_CASTER(QFlags<_T_>, const_name("QFlags<_T_>"));


    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      if(int_::check_(src))
      {
        value = _T_(pybind11::cast<long>(src));
        return true;
      }
      return false;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(QFlags<_T_> src, return_value_policy policy, handle parent ) {
      return pybind11::cast(int(src), policy, parent);
    }
  };

  template <typename Type, typename Key, typename Value>
  struct qt_map_caster
  {
      using key_conv   = make_caster<Key>;
      using value_conv = make_caster<Value>;

      bool load(handle src, bool convert) {
          if (!isinstance<dict>(src))
              return false;
          auto d = reinterpret_borrow<dict>(src);
          value.clear();
          for (auto it : d) {
              key_conv kconv;
              value_conv vconv;
              if (!kconv.load(it.first.ptr(), convert) ||
                  !vconv.load(it.second.ptr(), convert))
                  return false;
              value[cast_op<Key &&>(std::move(kconv))] = cast_op<Value &&>(std::move(vconv));
          }
          return true;
      }

      template <typename T>
      static handle cast(T &&src, return_value_policy policy, handle parent) {
          dict d;
          return_value_policy policy_key = policy;
          return_value_policy policy_value = policy;
          if (!std::is_lvalue_reference<T>::value) {
              policy_key = return_value_policy_override<Key>::policy(policy_key);
              policy_value = return_value_policy_override<Value>::policy(policy_value);
          }
          for (auto it = src.begin(); it != src.end(); ++it) {
              auto key = reinterpret_steal<object>(key_conv::cast(forward_like<T>(it.key()), policy_key, parent));
              auto value = reinterpret_steal<object>(value_conv::cast(forward_like<T>(it.value()), policy_value, parent));
              if (!key || !value)
                  return handle();
              d[key] = value;
          }
          return d.release();
      }

      PYBIND11_TYPE_CASTER(Type, const_name("Dict[") + key_conv::name + const_name(", ") + value_conv::name + const_name("]"));
  };

  template <typename Key, typename Value>
  struct type_caster<QHash<Key, Value>>
    : qt_map_caster<QHash<Key, Value>, Key, Value> { };
  template <typename Key, typename Value>
  struct type_caster<QMap<Key, Value>>
    : qt_map_caster<QMap<Key, Value>, Key, Value> { };
  template <typename Type> struct type_caster<QList<Type>>
    : list_caster<QList<Type>, Type> { };

}
