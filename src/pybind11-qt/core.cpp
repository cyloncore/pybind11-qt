#include "core.h"

namespace pybind11_qt
{
  static QList<const detail::abstract_qvariant_converter*> converters_;

  struct Initialiser
  {
    Initialiser();
  };
}

pybind11_qt::Initialiser::Initialiser()
{
  pybind11_qt::register_qvariant_converter<double>();
  pybind11_qt::register_qvariant_converter<QString>();
  pybind11_qt::register_qvariant_converter<QVariantMap>();
  pybind11_qt::register_qvariant_converter<QVariantList>();
}

pybind11_qt::Initialiser initialiser;

void pybind11_qt::detail::qvariant_converter_interface::register_qvariant_converter(abstract_qvariant_converter* _converter)
{
  converters_.append(_converter);
}

QList<const pybind11_qt::detail::abstract_qvariant_converter*> pybind11_qt::detail::qvariant_converter_interface::converters()
{
  return converters_;
}
